// 4.1.2 Reflect.defineMetadata ( metadataKey, metadataValue, target, propertyKey )
// https://rbuckton.github.io/reflect-metadata/#reflect.definemetadata

import "../src/Reflect";


describe("Reflect.defineMetadata", () => {
    it("InvalidTarget", () => {
        // @ts-expect-error
        expect(() => Reflect.defineMetadata("key", "value", undefined, undefined)).toThrow(TypeError);
    });

    it("ValidTargetWithoutTargetKey", () => {
        expect(() => Reflect.defineMetadata("key", "value", { })).not.toThrowError();
    });

    it("ValidTargetWithTargetKey", () => {
        expect(() => Reflect.defineMetadata("key", "value", { }, "name")).not.toThrowError();;
    });
});