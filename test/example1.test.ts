import "../src/Reflect";

describe("class metadata", () => {
    it("works", () => {

        
        class Base {

            @Reflect.metadata("mykey", "myvalue")
            method(arg1: string, arg2: number): [ string, number ] {
                return [ arg1, arg2 ]
            }

        }

        console.log(Reflect.getMetadataKeys(Base.prototype, "method"))
        console.log(Reflect.getMetadata('design:returntype', Base.prototype, "method"))
        console.log(Reflect.getMetadata('design:paramtypes', Base.prototype, "method"))
        console.log(Reflect.getMetadata('design:type', Base.prototype, "method"))
        console.log(Reflect.getMetadata('mykey', Base.prototype, "method"))
    })


    it("works2", () => {

        
        class Base {
        
            @Reflect.metadata("mykey", "myvalue")
            public static staticmethod(arg1: string, arg2: number): [ string, number ] {
                return [ arg1, arg2 ]
            }

        }


        console.log(Reflect.getMetadataKeys(Base, "staticmethod"))
        console.log(Reflect.getMetadata('design:returntype', Base, "staticmethod"))
        console.log(Reflect.getMetadata('design:paramtypes', Base, "staticmethod"))
        console.log(Reflect.getMetadata('design:type', Base, "staticmethod"))
        console.log(Reflect.getMetadata('mykey', Base, "staticmethod"))
    })
});
