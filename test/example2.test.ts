import { PropertyMetadata, required } from "../src/Reflect.helpers";

describe("class metadata", () => {
    it("works", () => {

        
        class Base {

            @PropertyMetadata("mykey", "myvalue")
            method(@required arg1: string, arg2: number): [ string, number ] {
                return [ arg1, arg2 ]
            }

        }

        console.log(Reflect.getMetadataKeys(Base.prototype))
        console.log(Reflect.getMetadata('metadata:keys', Base.prototype))

        console.log(Reflect.getMetadataKeys(Base.prototype, "method"))
        console.log(Reflect.getMetadata('design:returntype', Base.prototype, "method"))
        console.log(Reflect.getMetadata('design:paramtypes', Base.prototype, "method"))
        console.log(Reflect.getMetadata('design:type', Base.prototype, "method"))
        console.log(Reflect.getMetadata('mykey', Base.prototype, "method"))
    })



});
