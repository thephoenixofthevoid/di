import "./Reflect";

export type Newable<T> = { new(): T }

export function getOrDefineKeysSet<T>(metadataKey: any, target: any, getter: (target: any) => T) {
    if (!Reflect.hasOwnMetadata(metadataKey, target)) {
        Reflect.defineMetadata(metadataKey, getter(target), target)
    }
    return Reflect.getOwnMetadata(metadataKey, target)
}


export function definePropertyMetadata(metadataKey: any, metadataValue: any, target: any, propertyKey: string | symbol): void {
    const getter = () => new Set<any>()
    getOrDefineKeysSet("metadata:keys", target, getter).add(propertyKey)
    Reflect.defineMetadata(metadataKey, metadataValue, target, propertyKey)
}

export function PropertyMetadata(metadataKey: any, metadataValue: any) {
    function decorator(target: any, propertyKey: string | symbol): void {
        definePropertyMetadata(metadataKey, metadataValue, target, propertyKey);
    }
    return decorator;
}

const requiredMetadataKey = Symbol("required");
export function required(target: Object, propertyKey: string | symbol, parameterIndex: number) {
    let existingRequiredParameters: number[] = Reflect.getOwnMetadata(requiredMetadataKey, target, propertyKey) || [];
    existingRequiredParameters.push(parameterIndex);
    Reflect.defineMetadata( requiredMetadataKey, existingRequiredParameters, target, propertyKey);
  }